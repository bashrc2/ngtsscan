NGTSscan
========
This utility takes log files produced by NGTS and searches for exoplanet transits within them, producing plots if any are found. A range of possible orbital periods can be given, or if the period is already known then that can be specified.

For more information see:

    https://ngtransits.org

Installation
------------
Installation is pretty conventional:

    sudo apt-get install build-essential gnuplot python3-astropy
    make
    sudo make install

Obtaining Data
--------------
You will also need to obtain the NGTS **fits** files. These can be ontained from:

    https://archive.eso.org/scienceportal/home?data_collection=NGTS

Once you've downloaded some fits files you then need to convert them to **tbl** format which **ngts** can read:

    fits2ngtstbl "ADP.YYYY-MM-DDTHH mm ss.NNN.fits"

This will extract various source files in **tbl** format from the fits file and save them to the current directory.	

Usage
-----
To search for a transit within a range of orbital periods:

    ngtsscan -f ADP.YYYY-MM-DDTHH:mm:ss.NNN.tbl --min 2.0 --max 2.1

The above will search a particular log file for orbits in the range 2.0 to 2.1 days. If a transit is found then it will be plotted as **png** files saved to the current directory.

    > 10554 values loaded
    > orbital_period_days 2.075600

We found one! We can view it with:

    shotwell ADP.YYYY-MM-DDTHH:mm:ss.NNN.png

But it doesn't look quite right. We can manually tweak around with the orbital period to get the light curve as focussed as possible:

    ngtsscan -f ADP.YYYY-MM-DDTHH:mm:ss.NNN.tbl --period 2.07592
    shotwell ADP.YYYY-MM-DDTHH:mm:ss.NNN_distr.png

And also increase the vertical scale a little:

    ngtsscan -f ADP.YYYY-MM-DDTHH:mm:ss.NNN.tbl --period 2.07592 --vscale 1.4
    shotwell ADP.YYYY-MM-DDTHH:mm:ss.NNN_distr.png

Scanning Multiple Files
-----------------------
If you want to scan multiple **tbl** files within a directory there's also a helper script for that purpose:

    ngtsscandir --dir data --min [minimum period] --max [maximum period]

Other parameters can also be specified. See the **--help** option for details.

Log files will be scanned one by one and if transits are found then plot images will be generated for them within the same directory for subsequent manual review.
