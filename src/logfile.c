/*
  NGTSscan: Detection of exoplanet transits
  Copyright (C) 2021 Bob Mottram
  bob@libreserver.org

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* field index */
#define SOURCE_ID    0  /* Observation ID */
#define HJD          1  /* Date (days) */
#define FLUX         2  /* Processed flux from dithered stacked frame. (ADU/s) */
#define FLUX_ERR     3  /* Processed flux error from dithered stacked frame. (ADU/s) */
#define FLAG         4  /* Bitmask */

#include "ngtsscan.h"

/**
 * @brief Returns the number of lines in a log file
 * @param filename Log filename
 * @returns The number of lines
 */
static int lines_in_logfile(char * filename)
{
    FILE * fp;
    char linestr[512];
    int lines = 0;

    fp = fopen(filename,"r");
    if (!fp) return lines;

    while (!feof(fp)) {
        (void)(fgets(linestr,511,fp)+1);
        lines++;
    }
    return lines;
}

/**
 * @brief Loads a NGTS log file containing times and magnitudes
 *        for a given star
 * @param filename Log filename
 * @param timestamp Returned array containing the time of each data point
 * @param series Returned array containing magnitudes
 * @param max_series_length The maximum number of data points to be returned
 * @param time_field_index Index of the table column which contains the time
 * @param flux_field_index Index of the table column which contains the
 *        photon flux
 * @returns The number of data points loaded
 */
int logfile_load(char * filename, float timestamp[],
                 float series[], int max_series_length,
                 int time_field_index, int flux_field_index)
{
    FILE * fp;
    char linestr[512], valuestr[512];
    char * retval = NULL;
    int i,ctr=0,field_index;
    int series_length=0;
    /* number of lines in the tbl file */
    int lines = lines_in_logfile(filename);
    /* step size and current step count */
    int step = 1, stepctr = 0;

    printf("%d lines in log file\n", lines);

    /* if the number of lines in the file is bigger than the
       maximum array size, then calculate a step size for subsampling */
    if (lines > MAX_LOG_LINES) {
        step = lines / MAX_LOG_LINES;
        if (step < 1) step = 1;
    }

    fp = fopen(filename,"r");
    if (!fp) return -1;

    /* current step size */
    int stepincr = step;
    /* interval for randomness in the sampling step size */
    int randstep = step*2;

    while (!feof(fp)) {
        retval = fgets(linestr,511,fp);
        if (retval == NULL) continue;
        if (strlen(linestr) < 1) continue;
        if ((linestr[0] == '\\') ||
                (linestr[0] == '|')) {
            continue;
        }

        /* To avoid loading all samples we only store a sparser version
           with some randomness in the step size */
        stepctr++;
        if (stepctr >= stepincr) {
            /* reset the step count */
            stepctr = 0;
            /* calculate the next step size as a random value */
            if (step > 1)
                stepincr = (rand() % randstep);
            else
                stepincr = 1;
        }
        /* only record when the step count is at zero */
        if (stepctr > 0) continue;

        field_index = 0;
        ctr = 0;
        for (i = 0; i < strlen(linestr); i++) {
            if (!((linestr[i]==' ') || (linestr[i]=='\t') || (linestr[i]=='\n'))) {
                valuestr[ctr++] = linestr[i];
                continue;
            }
            if (ctr == 0) continue;
            valuestr[ctr] = 0;
            if (field_index == time_field_index) {
                if (valuestr[0] == 'n') break; /* nan */
                timestamp[series_length] = atof(valuestr);
            }
            else if (field_index == flux_field_index) {
                if (valuestr[0] == 'n') break; /* nan */
                if (atof(valuestr) > 30000) break; /* too large */
                series[series_length++] = atof(valuestr);
                if (series_length >= max_series_length) {
                    fclose(fp);
                    return(series_length);
                }
                break;
            }
            ctr = 0;
            field_index++;
        }
    }

    fclose(fp);
    return series_length;
}
